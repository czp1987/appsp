package com.anji.plus.ajpushlibrary.service;

import android.text.TextUtils;

import com.anji.plus.ajpushlibrary.AppSpPushLog;
import com.anji.plus.ajpushlibrary.base.AppSpPushBaseHandler;
import com.anji.plus.ajpushlibrary.model.AppSpModel;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Copyright © 2018 anji-plus
 * 安吉加加信息技术有限公司
 * http://www.anji-plus.com
 * All rights reserved.
 * <p>
 * 解析服务器返回的json串
 * </p>
 */
public class AppSpPushHandler extends AppSpPushBaseHandler {
    private IAppSpCallback appSpCallback;

    public void setAppSpCallback(IAppSpCallback appSpNoticeCallback) {
        this.appSpCallback = appSpNoticeCallback;
    }

    /**
     * 网络请求成功成功
     */
    public void handleSuccess(String data) {
        if (appSpCallback != null) {
            synchronized (appSpCallback) {
                //Notice
                try {
                    AppSpModel<String> spVersionModel = new AppSpModel<>();
                    JSONObject jsonObject = new JSONObject(data);
                    spVersionModel.setRepCode(getStringElement(jsonObject.opt("repCode")));
                    spVersionModel.setRepMsg(getStringElement(jsonObject.opt("repMsg")));
                    Object repDtaObj = jsonObject.opt("repData");
                    if (repDtaObj != null && !TextUtils.isEmpty(repDtaObj.toString())
                            && !"null".equalsIgnoreCase(repDtaObj.toString())) {
                        JSONArray repData = new JSONArray(repDtaObj.toString());
                    }
                    AppSpPushLog.d("通知返回客户端数据 " + spVersionModel);
                } catch (Exception e) {
                    AppSpPushLog.d("通知返回客户端数据 Exception e " + e.toString());
                }

            }
        }
    }

    /**
     * 数据上传异常
     *
     * @param code
     * @param msg
     */
    public void handleExcption(String code, String msg) {
        if (appSpCallback != null) {
            synchronized (appSpCallback) {
                appSpCallback.error(code, msg);
            }
        }
    }

}
