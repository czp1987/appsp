package com.anji.plus.ajpushlibrary.base;

public class AppSpPushBaseHandler {
    protected String getStringElement(Object object) {
        if (object instanceof String) {
            return (String) object;
        }
        return "";
    }

    protected Boolean getBooleanElement(Object object) {
        if (object instanceof Boolean) {
            return (Boolean) object;
        }
        return false;
    }

}
