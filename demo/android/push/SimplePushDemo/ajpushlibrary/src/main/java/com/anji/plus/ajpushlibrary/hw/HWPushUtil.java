package com.anji.plus.ajpushlibrary.hw;

import android.content.Context;
import android.text.TextUtils;

import com.anji.plus.ajpushlibrary.AppSpPushConfig;
import com.anji.plus.ajpushlibrary.AppSpPushLog;
import com.anji.plus.ajpushlibrary.AppSpPushConstant;
import com.anji.plus.ajpushlibrary.model.AppSpModel;
import com.anji.plus.ajpushlibrary.service.IAppSpCallback;
import com.huawei.agconnect.config.AGConnectServicesConfig;
import com.huawei.hmf.tasks.OnCompleteListener;
import com.huawei.hmf.tasks.Task;
import com.huawei.hms.aaid.HmsInstanceId;
import com.huawei.hms.common.ApiException;
import com.huawei.hms.push.HmsMessaging;

public class HWPushUtil {
    private Context context;

    public HWPushUtil(Context context) {
        this.context = context;
    }

    //获取token
    public void getToken() {
        // 创建一个新线程
        new Thread() {
            @Override
            public void run() {
                try {
                    // 从agconnect-service.json文件中读取appId
                    String appId = AGConnectServicesConfig.fromContext(context).getString("client/app_id");

                    // 输入token标识"HCM"
                    String tokenScope = "HCM";
                    String token = HmsInstanceId.getInstance(context).getToken(appId, tokenScope);
                    AppSpPushLog.i("get token: " + token);

                    // 判断token是否为空
                    if (!TextUtils.isEmpty(token)) {
                        AppSpPushConstant.pushToken = token;
                        //将获取的pushToken上传给服务器
                        AppSpPushConfig.getInstance().sendRegTokenToServer(new IAppSpCallback() {
                            @Override
                            public void pushInfo(AppSpModel<String> appSpModel) {

                            }

                            @Override
                            public void error(String code, String msg) {

                            }
                        });
                    }
                } catch (ApiException e) {
                    AppSpPushLog.e("get token failed, " + e);
                }
            }
        }.start();
    }

    //注销token
    public void deleteToken() {
        // 创建一个新线程
        new Thread() {
            @Override
            public void run() {
                try {
                    // 从agconnect-service.json文件中读取appId
                    String appId = AGConnectServicesConfig.fromContext(context).getString("client/app_id");

                    // 输入token标识"HCM"
                    String tokenScope = "HCM";

                    // 注销Token
                    HmsInstanceId.getInstance(context).deleteToken(appId, tokenScope);
                    AppSpPushLog.i("token deleted successfully");
                } catch (ApiException e) {
                    AppSpPushLog.e("deleteToken failed." + e);
                }
            }
        }.start();
    }

    //显示通知栏消息(系统默认是允许显示通知栏消息)
    public void TurnOnPush() {
        // 设置显示通知栏消息
        HmsMessaging.getInstance(context).turnOnPush().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(Task<Void> task) {
                // 获取结果
                if (task.isSuccessful()) {
                    AppSpPushLog.i("turnOnPush Complete");
                } else {
                    AppSpPushLog.e("turnOnPush failed: ret=" + task.getException().getMessage());
                }
            }
        });
    }

    //隐藏通知栏消息
    public void turnOffPush() {
        HmsMessaging.getInstance(context).turnOffPush().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(Task<Void> task) {
                // 获取结果
                if (task.isSuccessful()) {
                    AppSpPushLog.i("turnOffPush Complete");
                } else {
                    AppSpPushLog.e("turnOffPush failed: ret=" + task.getException().getMessage());
                }
            }
        });
    }

    //订阅主题
    public void subscribe(String topic) {
        try {
            // 主题订阅
            HmsMessaging.getInstance(context).subscribe(topic)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(Task<Void> task) {
                            // 获取主题订阅的结果
                            if (task.isSuccessful()) {
                                AppSpPushLog.i("subscribe topic successfully");
                            } else {
                                AppSpPushLog.e("subscribe topic failed, return value is " + task.getException().getMessage());
                            }
                        }
                    });
        } catch (Exception e) {
            AppSpPushLog.e("subscribe failed, catch exception : " + e.getMessage());
        }
    }

    //退订主题
    public void unsubscribe(String topic) {
        try {
            // 取消主题订阅
            HmsMessaging.getInstance(context).unsubscribe(topic)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(Task<Void> task) {
                            // 获取取消主题订阅的结果
                            if (task.isSuccessful()) {
                                AppSpPushLog.i("unsubscribe topic successfully");
                            } else {
                                AppSpPushLog.e("subscribe topic failed, return value is " + task.getException().getMessage());
                            }
                        }
                    });
        } catch (Exception e) {
            AppSpPushLog.e("subscribe failed, catch exception : " + e.getMessage());
        }
    }
}


