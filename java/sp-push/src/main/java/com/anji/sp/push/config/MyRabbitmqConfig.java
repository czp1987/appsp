package com.anji.sp.push.config;

import com.anji.sp.push.constants.MqConstants;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: Kean
 * @Date: 2021/1/20
 * @Description:
 */
@Configuration
public class MyRabbitmqConfig {
    //创建交换机
    @Bean
    public DirectExchange defDirectExchange() {
        return new DirectExchange(MqConstants.directExchange);
    }

    // 创建一个direct的队列
    @Bean
    public Queue createDirectBatchQueue() {
        return new Queue(MqConstants.directBatchQueue, true);
    }

    // 创建一个direct的队列
    @Bean
    public Queue createDirectAllQueue() {
        return new Queue(MqConstants.directAllQueue, true);
    }

    //队列与交换机进行绑定
    @Bean
    public Binding bindingSingleQuene() {
        return BindingBuilder.bind(createDirectBatchQueue()).to(defDirectExchange()).with(MqConstants.routing);
    }
    //队列与交换机进行绑定
    @Bean
    public Binding bindingAllQueue() {
        return BindingBuilder.bind(createDirectAllQueue()).to(defDirectExchange()).with(MqConstants.routing);
    }

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        return new RabbitTemplate(connectionFactory);
    }
}
