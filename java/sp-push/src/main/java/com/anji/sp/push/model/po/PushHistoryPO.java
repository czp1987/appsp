package com.anji.sp.push.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;
import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 推送历史表
 * </p>
 *
 * @author kean
 * @since 2021-01-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("push_history")
public class PushHistoryPO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 应用唯一key
     */
    private String appKey;

    /**
     * 设备唯一标识
     */
    private String deviceIds;
    /**
     * 极光唯一标识
     */
    private String registrationIds;
    /**
     * 消息唯一id
     */
    private String msgId;

    /**
     * 1:华为，2:小米，3:oppo，4:vivo，5：极光iOS 6：极光Android
     */
    private String targetType;

    /**
     * 目标（对应当次发送的条数）
     */
    private String targetNum;

    /**
     * 成功条数（对应当次发送的条数）
     */
    private String successNum;

    /**
     * 标题
     */
    private String title;

    /**
     * 内容
     */
    private String content;

    /**
     * 传递数据
     */
    private String extras;

    /**
     * iOS配置项 json {"sound": "sound"}
     */
    private String iosConfig;
    /**
     * android配置项 json {"sound": "sound"}
     */
    private String androidConfig;

    /**
     * 消息来源 0 web 1 api
     */
    private String sendOrigin;

    /**
     * 推送类型 1透传消息 0 普通消息
     */
    private String pushType;

    /**
     * 渠道类型 华为厂商还是其他厂商
     */
    private String channel;

    /**
     * 接收结果是否成功 1成功 0不成功
     */
    private String pushResult;

    /**
     * 发送时间
     */
    private LocalDateTime sendTime;

    /**
     * 0--已禁用 1--已启用  DIC_NAME=ENABLE_FLAG
     */
    private Integer enableFlag;

    /**
     * 0--未删除 1--已删除 DIC_NAME=DEL_FLAG
     */
    private Integer deleteFlag;

    /**
     * 创建者
     */
    private Long createBy;

    /**
     * 创建时间
     */
    private LocalDateTime createDate;

    /**
     * 更新者
     */
    private Long updateBy;

    /**
     * 更新时间
     */
    private LocalDateTime updateDate;

    /**
     * 备注信息
     */
    private String remarks;


}
