package com.anji.sp.push.controller;

import com.anji.sp.model.ResponseModel;
import com.anji.sp.push.model.vo.*;
import com.anji.sp.push.service.PushHistoryService;
import com.anji.sp.push.service.impl.JPushServiceImpl;
import com.anji.sp.push.service.impl.VivoPushServiceImpl;
import com.anji.sp.push.service.impl.XiaomiPushServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * 推送结果回调控制器
 */
@RestController
@RequestMapping("/push")
@Api(tags = "推送结果回调接口")
@Slf4j
public class PushResultController {
    @Autowired
    private PushHistoryService pushHistoryService;

    @Autowired
    private JPushServiceImpl jPushService;
    @Autowired
    private XiaomiPushServiceImpl xiaomiPushService;
    @Autowired
    private VivoPushServiceImpl vivoPushService;
    @ApiOperation(value = "华为推送根据msgIds结果回调", httpMethod = "POST")
    @PostMapping("/getReceiveIdsDetail")
    public ResponseModel getReceiveIdsDetail(@RequestBody PushMsgReceiveVO pushMsgReceiveVO){
        ResponseModel receiveIdsDetail = jPushService.getReceiveIdsDetail(pushMsgReceiveVO.getJgMsgIds(), pushMsgReceiveVO.getJgMasterSecret(), pushMsgReceiveVO.getJgAppKey());
        return receiveIdsDetail;
    }
    //
    @ApiOperation(value = "小米推送根据msgIds结果回调", httpMethod = "POST")
    @PostMapping("/getXiaoMiMessageStatus")
    public ResponseModel getXiaoMiMessageStatus(@RequestBody PushMsgReceiveVO pushMsgReceiveVO){
        ResponseModel receiveIdsDetail = xiaomiPushService.getXiaoMiMessageStatus(pushMsgReceiveVO.getXmMsgIds(), pushMsgReceiveVO.getXmAppSecret());
        return receiveIdsDetail;
    }

    @ApiOperation(value = "vivo推送根据msgIds结果回调", httpMethod = "POST")
    @PostMapping("/getVivoMiMessageStatus")
    public ResponseModel getVivoMiMessageStatus(@RequestBody PushMsgReceiveVO pushMsgReceiveVO){
        ResponseModel receiveIdsDetail = vivoPushService.getVivoMiMessageStatus(pushMsgReceiveVO.getVivoTaskIds(), pushMsgReceiveVO.getVoAppSecret(), pushMsgReceiveVO.getVoAppId(), pushMsgReceiveVO.getVoAppKey());
        return receiveIdsDetail;
    }

    //需要在web开通回执功能 并上传https证书（PEM格式） 在web配置回调地址
    @ApiOperation(value = "华为推送结果回调", httpMethod = "POST")
    @PostMapping("/pushHuaweiCallBack")
    public ResponseModel pushHuaweiCallBack(@RequestBody Map<String, HuaweiCallBackBean> map) {
        log.info("华为推送结果回调:" + map);
        //华为推送服务器调用我们接口把接收结果反馈给我们
        //根据传过来的参数拿到成功接收的用户数 根据msgId保存到消息历史记录中
        if (map == null) {
            return ResponseModel.errorMsg("参数缺失");
        }
        Set set = map.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry<String, HuaweiCallBackBean> entry1 = (Map.Entry<String, HuaweiCallBackBean>) i.next();
            String state = entry1.getKey();
            HuaweiCallBackBean huaweiCallBackBean = entry1.getValue();

            //根据taskId 查询数据库中有没有这个任务 如果没有就不操作 如果有就更新pushHistoryService       把huaweiCallBackBean中的state设置为成功是否


        }
        return ResponseModel.success();
    }

    @ApiOperation(value = "小米推送结果回调", httpMethod = "POST")
    @PostMapping("/pushXiaomiCallBack")
    public ResponseModel pushXiaomiCallBack(@RequestBody Map<String, XiaomiCallBackBean> map) {
        log.info("小米推送结果回调:" + map);
        if (map == null) {
            return ResponseModel.errorMsg("参数缺失");
        }
        Set set = map.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry<String, XiaomiCallBackBean> entry1 = (Map.Entry<String, XiaomiCallBackBean>) i.next();
            String msgId = entry1.getKey();
            XiaomiCallBackBean xiaomiCallBackBean = entry1.getValue();
            //根据taskId 查询数据库中有没有这个任务 如果没有就不操作 如果有就更新pushHistoryService       把xiaomiCallBackBean中的targets个数设置为成功数


        }
        return ResponseModel.success();
    }

    @ApiOperation(value = "oppo推送结果回调", httpMethod = "POST")
    @PostMapping("/pushOppoCallBack")
    public ResponseModel pushOppoCallBack(@RequestBody ArrayList<OppoCallBackBean> list) {

        log.info("oppo推送结果回调:" + list);

        if (list == null||list.size()==0) {
            return ResponseModel.errorMsg("参数缺失");
        }

        for (int i=0;i<list.size();i++){
            String messageId=list.get(i).getMessageId();
            String registrationIds=list.get(i).getRegistrationIds();
        }
        return ResponseModel.success();
        //暂时没找到回调
    }

    @ApiOperation(value = "Vivo推送结果回调", httpMethod = "POST")
    @PostMapping("/pushVivoCallBack")
    public ResponseModel pushVivoCallBack(@RequestBody Map<String, VivoCallBackBean> map) {
        log.info("Vivo推送结果回调:" + map);

        if (map == null) {
            return ResponseModel.errorMsg("参数缺失");
        }
        System.out.println("Vivo推送结果回调:" + map.toString());
        Set set = map.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry<String, VivoCallBackBean> entry1 = (Map.Entry<String, VivoCallBackBean>) i.next();
            String taskId = entry1.getKey();
            VivoCallBackBean vivoCallBackBean = entry1.getValue();
            //根据taskId 查询数据库中有没有这个任务 如果没有就不操作 如果有就更新pushHistoryService       把vivoCallBackBean中的targets个数设置为成功数


        }
        return ResponseModel.success();
    }







}
